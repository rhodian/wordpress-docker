### Starting

 1. Make sure you're at the root, where this README is!
 1. Run `$ scripts/run.sh`, and then go to localhost:8000. The changes are reflected immediately.
 1. If you made any changes to the docker-compose file, run `$ scripts/rebuild`

### Stopping

 1. Run `$ scripts/stop.sh` to stop and remove all containers defined in the docker-compose.yml file.
     - Note that the persistent volume for the database will still exist even afetr the stop. You'll need to run `$ docker-compose down -v` with that -v to delete that volume. 
